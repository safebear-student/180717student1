package com.safebear.app;

import org.junit.Test;


import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 18/07/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin() {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(this.welcomePage.checkCorrectPage());

        //Step 2 Click on the Login link and the Login Page loads
        assertTrue(this.welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
